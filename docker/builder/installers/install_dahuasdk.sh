#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_dahuasdk {
    local url="https://zhilu.bj.bcebos.com/dahua-sdk.tar.gz"
    local sha256="2dc522b4c20dc2a8c505c42df1f531d3f35a8c05703e26f7adcfaa4ed9204951"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "dahua-sdk.tar.gz" "${sha256}"; then
            tar -zxf dahua-sdk.tar.gz
        else
            echo "check_sha256 failed: dahua-sdk.tar.gz"
        fi
        rm dahua-sdk.tar.gz
    popd >/dev/null
}

function main {
    install_dahuasdk
}

main "$@"