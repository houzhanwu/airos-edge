#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_yaml {
    local url="https://zhilu.bj.bcebos.com/yaml-cpp.tar.gz"
    local sha256="5b5dabe8227d25c377750dd3274a93f11348382ecc48b0b6dda183ad2c30bc2b"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "yaml-cpp.tar.gz" "${sha256}"; then
            tar -zxf yaml-cpp.tar.gz
        else
            echo "check_sha256 failed: yaml-cpp.tar.gz"
        fi
        rm yaml-cpp.tar.gz
    popd >/dev/null
}

function main {
    install_yaml
}

main "$@"