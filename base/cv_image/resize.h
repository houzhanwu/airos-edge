/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <cuda_runtime_api.h>

#include "base/blob/blob.h"
#include "base/cv_image/image.h"
#include "glog/logging.h"

namespace airos {
namespace base {

bool ResizeGPU(const airos::base::Image8U &src,
               std::shared_ptr<airos::base::Blob<float> > dst, int stepwidth,
               int start_axis, cudaStream_t stream = nullptr);

bool ResizeGPU(const airos::base::Blob<uint8_t> &src_gpu,
               std::shared_ptr<airos::base::Blob<float> > dst, int stepwidth,
               int start_axis, int mean_b, int mean_g, int mean_r,
               bool channel_axis, float scale, cudaStream_t stream = nullptr);

bool ResizeGPU(const airos::base::Image8U &src,
               std::shared_ptr<airos::base::Blob<float> > dst, int stepwidth,
               int start_axis, float mean_b, float mean_g, float mean_r,
               bool channel_axis, float scale, cudaStream_t stream = nullptr);

bool ResizeGPU(const airos::base::Image8U &src,
               std::shared_ptr<airos::base::Blob<float> > dst, int stepwidth,
               int start_axis, float mean_b, float mean_g, float mean_r,
               bool channel_axis, float scale_b, float scale_g, float scale_r,
               cudaStream_t stream = nullptr);

// bool GpuMatResize(void* res_ptr, int height, int width, void* src_ptr, int
// src_height, int src_width);

}  // namespace base
}  // namespace airos
