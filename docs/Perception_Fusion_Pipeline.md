# Perception Fusion Pipeline


## 1.多传感器障碍物融合框图
![多传感器障碍物融合框图](./images/perception_fusion_pipeline.png)
## 2.多传感器障碍物融合类图
![多传感器障碍物融合类图](./images/perception_fusion_uml.png)
## 3.快速开始
融合依赖于单相机感知进程的输出,单相机拉起方式: TODO

多传感器融合进程拉起:
```
    cyber_launch start air_service/launch/fusion.launch
```

多传感器融合可视化进程拉起:
```
    cyber_launch start air_service/launch/fusion_visualization.launch
```

## 4.模块介绍
### 4.1 目录结构
perception-fusion\
├── algorithm\
│   ├── air_fusion\
│   │   ├── base\
│   │   └── fusion\
│   └── interface\
├── conf\
├── dag\
├── pipeline\
│   └── tools\
├── proto\
└── visualization

- perception-fusion/algorithm 包括融合算法接口和融合算法实现
- perception-fusion/conf 包括融合app的配置以及算法的配置
- perception-fusion/dag 通过cyberrt中间件拉起融合进程的启动脚本
- perception-fusion/pipeline 融合pipeline
- perception-fusion/proto 对 perception-fusion/conf 中配置文件的定义
- perception-fusion/visualization 融合可视化

### 4.2 融合算法
车路协同可以解决单车智能遇到的困难问题，减少接管次数，降低单车成本，甚至实现基于纯路侧感知自动驾驶，实现这个的能力要求是对所有交通参与者进行感知理解，感知精度达到亚米级，所提供路侧感知信息可实现高级别自动驾驶决策闭环。路侧感知系统融合框架解决了误关联、位置精度以及车端应用的疼点，其中关联创新性的提出基于误差建模概率分布结合轨迹、车道以及尺寸等多维度的联合概率方案，关联成功率大于98%。跟踪采用时间和空间相结合的卡尔曼滤波，将同一物体，不同传感器不同时刻的观测值进行滤波跟踪。

本次开源主要是基础融合关联部分以及整体框架，跟踪暂不开源（后续可关注进一步的开源内容）。3D 融合关联模块基于概率分布模型来建立关联矩阵，对于关联融合所需的所有维度的信息（障碍物位置、类别、尺寸、车道信息、轨迹等），通过其概率分布来得到融合概率分布，概率最高点则为融合后的结果，协方差则为本次融合结果的置信度及融合分布。该方案关键在于推导多维概率分布相乘后的分布模型，以及确定每一个维度分布形式并估计分布的参数，并对各个维度解耦，在各自维度上进行概率融合。每个维度的概率融合都会对关联进行check。关联内核：匈牙利匹配。

### 4.3 融合可视化
**实现方式:**

通过opencv的gui功能将障碍物位置和尺寸等信息显示到屏幕中。

**使用方法：**

可视化包括对输入和输出障碍物的可视化，一共分为三种模式，可在可视化图形界面的上方通过鼠标滑动选择，也可用使用键盘的左右健进行模式切换。

- mode 0: 输入障碍物

- mode 1: 输出障碍物

- mode 2: 输入和输出混合

可以通过鼠标勾选显示的障碍物信息，包括障碍物ID、相机ID、障碍物类型、障碍物子类型、误差圆范围等。

其他使用介绍：

- y: 重置画布大小
- w s a d：移动显示范围




>如果您需要使用智路OS开发者平台，或者在开发使用过程中遇到任何问题，欢迎通过以下方式联系我们：  
> Email：zhiluos@163.com