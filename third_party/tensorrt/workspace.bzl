def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "tensorrt",
        build_file = clean_dep("//third_party/tensorrt:tensorrt.BUILD"),
        path = "/opt/tensorrt"
    )