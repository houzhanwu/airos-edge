"""Loads the protobuf library"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def clean_dep(dep):
    return str(Label(dep))

def repo():
    http_archive(
        name = "com_google_protobuf",
        sha256 ="3ddaffddd73d86e4005cd304c4a4fd962c1819f4c6aa9aad1c3bb2c7f1a35647",
        strip_prefix = "protobuf-3.15.0",
        urls = [
            "https://zhilu.bj.bcebos.com/protobuf-cpp-3.15.0.tar.gz",
        ],
    )

def repo_import():
    native.new_local_repository(
        name = "protobuf_import",
        build_file = clean_dep("//third_party/protobuf:import.BUILD"),
        path = "output/protobuf"
    )